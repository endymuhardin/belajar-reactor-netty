package com.muhardin.endy.belajar.reactor.netty;

import io.netty.handler.codec.LineBasedFrameDecoder;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.NettyPipeline;
import reactor.netty.tcp.TcpServer;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class EchoServer {
    public static void main(String[] args) throws Exception {
        final CountDownLatch latch = new CountDownLatch(6);

        final TcpServer server = TcpServer.create()
                .port(12321)
                .doOnConnection(c -> c.addHandlerFirst("codec", new LineBasedFrameDecoder(8 * 1024)));

        DisposableServer disposableServer
                = server.handle((in, out) -> {
                    return out.options(NettyPipeline.SendOptions::flushOnEach)
                            .sendString(
                                in.receive()
                                .asString()
                                .log("server")
                                .map(data -> {
                                    System.out.println("From client : "+data);
                                    latch.countDown();
                                    return "Halo "+data+"\r\n";
                            })
                    );
        })
        .wiretap(true)
        .bindNow();

        System.out.println("Wait for 6 messages");
        latch.await();
        disposableServer.disposeNow();
    }
}
