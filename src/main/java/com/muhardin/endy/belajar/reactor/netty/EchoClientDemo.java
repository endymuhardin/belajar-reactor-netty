package com.muhardin.endy.belajar.reactor.netty;

import io.netty.handler.codec.LineBasedFrameDecoder;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.Connection;
import reactor.netty.NettyPipeline;
import reactor.netty.tcp.TcpClient;

import java.util.concurrent.CountDownLatch;

public class EchoClientDemo {
    public static void main(String[] args) throws Exception {
        final CountDownLatch latch = new CountDownLatch(2);

        EchoClient echoClient = new EchoClient("0:0:0:0:0:0:0:1", 12321, latch);
        echoClient.send(Mono.just("Endy\r\n"));
        echoClient.send(Mono.just("Violetta\r\n"));

        latch.await();
        echoClient.disconnect();
    }
}

class EchoClient {
    private Connection clientConnection;
    private CountDownLatch latch;
    private Subscriber<Void> subscriber;

    public EchoClient(String host, Integer port, CountDownLatch latch){
        this.latch = latch;
        final TcpClient client = TcpClient.create()
                .host(host)
                .port(port)
                .doOnConnected(c -> c.addHandlerFirst("codec", new LineBasedFrameDecoder(8 * 1024)));
        clientConnection = client.wiretap(true).connectNow();

        subscriber = new Subscriber<Void>(){
            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onNext(Void aVoid) {

            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
            }

            @Override
            public void onComplete() {
                System.out.println("Done");
            }
        };
    }

    public void send(Mono<String> msg){
        clientConnection.outbound()
                .options(NettyPipeline.SendOptions::flushOnEach)
                .sendString(msg)
                .then(clientConnection.inbound().receive()
                        .asString()
                        .log("client")
                        .doOnNext(s -> {
                            System.out.println(s);
                            latch.countDown();
                        })
                        .then())
                .subscribe(subscriber);

    }

    public void disconnect() {
        clientConnection.disposeNow();
    }
}